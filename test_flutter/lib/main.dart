import 'package:flutter/material.dart';
import 'package:test_flutter/ui/home.dart';


void main() => runApp(
  new MaterialApp(
    home : BillSplitter(),
  )
);

//void main() => runApp(
//  new MaterialApp(
//    home: Wisdom(),
//  ),
//);

// void main() => runApp(
//   new MaterialApp(
//     home: BizCards(),
//   ),
// );

// void main() => runApp(
//   new MaterialApp(
//     home: MyApp(),
//   ),
// );


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Scaffold"),
        centerTitle: true,
        backgroundColor: Colors.amberAccent.shade100,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.email),
            onPressed: () => print('email tapped'),
          ),
          IconButton(
            icon: Icon(Icons.access_alarm),
            onPressed: _tapButton,
          )
        ],
      ),
      
      floatingActionButton: FloatingActionButton(
        onPressed: () => debugPrint("Hello ... "),
        backgroundColor: Colors.lightGreen,
        child: Icon(Icons.call_missed),
      ),
      
      bottomNavigationBar: BottomNavigationBar(
        onTap: (int index) => debugPrint("tapped... : $index"),
        items: [
          BottomNavigationBarItem(
            title: Text("First"),
            icon: Icon(Icons.account_circle),
          ),
          BottomNavigationBarItem(
            title: Text("Second"),
            icon: Icon(Icons.ac_unit),
          ),
          BottomNavigationBarItem(
            title: Text("Third"),
            icon: Icon(Icons.access_alarms),
          ),
        ],
      ),
      backgroundColor: Colors.redAccent.shade50,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CustomButtom(),
            // InkWell(
            //   child: Text(
            //     "Tap me",
            //     style: TextStyle(fontSize: 24.3),
            //   ),
            //   onTap: () => debugPrint("tapped ..."),
            // ),
          ],
        ),
      ),
    );
  }

  _tapButton() {
    print("tapped access alarm");
  }
}

class CustomButtom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final snackBar = SnackBar(
          content: Text("Hello Again"),
        );
        Scaffold.of(context).showSnackBar(snackBar);
      },
      child: Container(
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: Colors.pinkAccent,
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Text("Button"),
      ),
    );
  }
}
