import 'package:flutter/material.dart';
import 'package:test_flutter/util/hexcolor.dart';

//Rappresenta una widget con al suo interno uno
//Scaffold.
class ScaffoldExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return null;
  }
}

class BillSplitter extends StatefulWidget {
  @override
  _BillSplitterState createState() => _BillSplitterState();
}

/**
 * Crea l'interfaccia principale.
 */
class _BillSplitterState extends State<BillSplitter> {
  int  _tipPercent = 0;
  int _personCounter = 1;
  double _billAmount = 0.0;
  Color _purple = HexColor("#6908D6");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        color: Colors.white,
        child: ListView(
          scrollDirection: Axis.vertical,
          padding: EdgeInsets.all(20.5),
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.1
              ),
              width: 150,
              height: 150,
              decoration: BoxDecoration(
                color: _purple.withOpacity(0.1),//Colors.purpleAccent.shade100,
                borderRadius: BorderRadius.circular(12.0),
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Total per Person",
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 15.0,
                        color: _purple,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('\$ ${calculateTotalPerPerson(_billAmount,
                          _personCounter,
                          _tipPercent)}',
                          style : TextStyle(
                        fontSize: 34.9,
                        fontWeight: FontWeight.bold,
                      )),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top : 20.9),
              padding: EdgeInsets.all(12.0),
              decoration: BoxDecoration(
                color: Colors.transparent,
                border: Border.all(
                  color: Colors.blueGrey.shade100,
                  style: BorderStyle.solid,
                ),
                borderRadius: BorderRadius.circular(
                  12.0,
                ),
              ),
              child: Column(
                children: <Widget>[
                  TextField(
                    keyboardType: TextInputType.numberWithOptions(
                      decimal: true,
                    ),
                    style: TextStyle(
                      color: _purple,
                    ),
                    decoration: InputDecoration(
                      prefixText: "Bill Amount",
                      prefixIcon: Icon(
                        Icons.attach_money,
                      )
                    ),
                    onChanged: (String value) {
                      try{
                        _billAmount = double.parse(value);
                      }catch (exception){
                        _billAmount = 0.0;
                      }
                    },
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Center(
                        child: Text("Split",style: TextStyle(
                          color: _purple,
                          fontWeight: FontWeight.bold,
                          fontSize: 17.0,
                        ),),
                      ),
                      Row(
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              setState(() {
                                if(_personCounter > 1){
                                  _personCounter --;
                                }else{
                                  //do nothing
                                }
                              });
                            },
                            child : Container(
                              width: 40.0,
                              height: 40.0,
                              margin: EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(7.0),
                                color: _purple.withOpacity(0.1),
                              ),
                              child: Center(
                                child: Text(
                                  "-",
                                  style: TextStyle(
                                    color: _purple,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Text("$_personCounter",
                            style: TextStyle(
                              color: _purple,
                              fontWeight: FontWeight.bold,
                              fontSize: 17.0,
                            ),),
                          InkWell(
                            onTap: () {
                              setState(() {
                                _personCounter++;

                              });
                            },
                            child: Container(
                              width: 40.0,
                              height: 40.0,
                              margin: EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                color: _purple.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(7.0),
                              ),
                              child: Center(
                                child: Text("+",style: TextStyle(
                                  color: _purple,
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.bold,
                                ),),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Tip",style: TextStyle(
                        color: Colors.grey.shade700,
                      ),),
                      Padding(
                        padding: const EdgeInsets.all(18.0),
                        child: Text(
                          "\$ ${calculateTotalTip(_billAmount, _personCounter, _tipPercent)}",
                          style: TextStyle(
                            color: _purple,
                            fontWeight: FontWeight.bold,
                            fontSize: 17.0
                          ),
                        ),
                      ),
                    ],
                  ),

                  //Slider
                  Column(
                    children: <Widget>[
                      Text(
                        "$_tipPercent %",
                        style: TextStyle(
                          color: _purple,
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Slider(
                        min: 0,
                        max: 100,
                        activeColor: _purple,
                        inactiveColor: Colors.grey,
                        divisions: 10,
                        onChanged: (double newValue){
                          setState(() {
                            _tipPercent = newValue.round();
                          });
                        },
                        value: _tipPercent.toDouble(),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  calculateTotalPerPerson(double billAmount , int splitBy , int tipPercentage) {
    double totalPerPerson = (calculateTotalTip(billAmount,splitBy, tipPercentage) + billAmount) / splitBy;


    return totalPerPerson.toStringAsFixed(2);
  }

  calculateTotalTip(double billAmount , int splitBy , int tipPercentage){
    double totalTip = 0.0;
    if(billAmount < 0 || billAmount.toString().isEmpty || billAmount == null){

    } else {
      totalTip = (billAmount * tipPercentage) / 100;
    }

    return totalTip;
  }
}




//classe personalizzata che rappresenta una widget
//per la gestione di un bottone personalizzato.
class CustomButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return null;
  }
}

//rappresenta il contenuto della proprietà home
//dello Scaffold
class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return null;
  }
}

class BizCards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BizCard"),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            _getCard(),
            _getAvatar(),
          ],
        ),
      ),
    );
  }

  Container _getCard() {
    return Container(
      width: 350,
      height: 200,
      margin: EdgeInsets.all(50.0),
      decoration: BoxDecoration(
        color: Colors.pinkAccent,
        borderRadius: BorderRadius.circular(14.5),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Andrea',
            style: TextStyle(
              fontSize: 20.9,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.italic,
              color: Colors.white,
            ),
          ),
          Text('Esempio di Test'),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.person_add),
              Text("T: @buildappswithme"),
            ],
          ),
        ],
      ),
    );
  }

  //Ritorna un contenitore con un avatar all'interno.
  Container _getAvatar() {
    return Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(50.0)),
          border: Border.all(color: Colors.redAccent, width: 1.2),
          image: DecorationImage(
              image: NetworkImage("https://picsum.photos/300/300"),
              fit: BoxFit.cover)),
    );
  }
}

class Wisdom extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _WidsomState();
}

class _WidsomState extends State<Wisdom> {
  int _index = 0;

  List quotes = [
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
        
            child : Container(child: Center(
                child: Text(quotes[_index],style: TextStyle(
                  fontStyle: FontStyle.italic,
                      fontSize: 18.8, 
                      color: Colors.white
                ),),
                
              ),
              width: 350,
              height: 200,
              margin: EdgeInsets.all(30.0),
              decoration: BoxDecoration(
                color : Colors.greenAccent,
                borderRadius: BorderRadius.circular(14.5),
              ),
            ),
          ),

          Divider(thickness: 2.3),
          Padding(
              padding: const EdgeInsets.only(top: 38.0),
              child: FlatButton.icon(
                  color: Colors.greenAccent.shade700,
                  onPressed: () => _showQuote(),
                  icon: Icon(Icons.wb_sunny),
                  label: Text(
                    "Inspire Me !!",
                    style: TextStyle(
                      fontSize: 18.8, color: Colors.white),
                  ))),
        ],
      ),
    ));
  }

  _showQuote() {
    setState(() {
      if (_index == quotes.length - 1) {
        _index = 0;
      } else {
        this._index++;
      }
    });
  }
}
