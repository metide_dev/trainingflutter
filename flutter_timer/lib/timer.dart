import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_timer/bloc/bloc.dart';

/*
 *  Questa classe ha uno stato interno in quanto alla notifica degli eventi di play
 * ,resume, reset ecc ecc si modificherà la visualizzazione della bottoniera e il valore del timer. 
 */
class Timer extends StatelessWidget {
  static const TextStyle timerTextStyle = TextStyle(
    fontSize: 60,
    fontWeight: FontWeight.bold,
  );

  /*
   * Disegnamo il timer in minuti e secondi.
   */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Flutter Timer')),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: 40.0),
            child: Center(
              //tutto all'interno di un bloc builder in quanto si applica il pattern
              //stati / eventi.
              child: BlocBuilder<TimerBloc, TimerState>(
                builder: (context, state) {
                  //data la durata in secondi, li converte nei minuti escludendo le i secondi
                  final String minutesStr = ((state.duration / 60) % 60)
                      .floor()
                      .toString()
                      .padLeft(2, '0');
                  //data la durata in secondi, converte nella 
                  final String secondsStr =
                      (state.duration % 60).floor().toString().padLeft(2, '0');
                  //concatena i risultati disegnando il timer mm:ss
                  return Text(
                    '$minutesStr:$secondsStr',
                    style: Timer.timerTextStyle,
                  );
                },
              ),
            ),
          ),
          //verifica se è stato notificato un evento, in questo modo
          //ridisegna i pulsanti.
          BlocBuilder<TimerBloc, TimerState>(
            condition: (previousState, state) =>
                state.runtimeType != previousState.runtimeType,
            builder: (context, state) => Actions(),
          ),
        ],
      ),
    );
  }
}

/*
 *  Si vuole creare una widget per gestire i bottoni che notificherrano
 *  gli eventi di pausa , resume.
 *  In funzione dello stato corrente allora visualizzerò un determinato pulsante.   
 */
class Actions extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: _mapStateToActionButtons(
        timerBloc: BlocProvider.of<TimerBloc>(context),
      ),
    );
  }

  List<Widget> _mapStateToActionButtons({TimerBloc timerBloc,}) {

    final TimerState currentState = timerBloc.state;
    //se il timer è fermo e pronto a partire visualizzerò solamente il pulsante di play
    if (currentState is Ready) {
      return [
        FloatingActionButton(
          child: Icon(Icons.play_arrow),
          onPressed: () =>
              timerBloc.add(Start(duration: currentState.duration)),
        ),
      ];
    }
    //se il timer è in esecuzione allora visualizzerò
    //il pulsante di pausa e di reset.
    if (currentState is Running) {
      return [
        FloatingActionButton(
          child: Icon(Icons.pause),
          onPressed: () => timerBloc.add(Pause()),
        ),
        FloatingActionButton(
          child: Icon(Icons.replay),
          onPressed: () => timerBloc.add(Reset()),
        ),
      ];
    }

    //se il timer è in pausa allora visualizzerò il pulsante di play e di reset.
    if (currentState is Paused) {
      return [
        FloatingActionButton(
          child: Icon(Icons.play_arrow),
          onPressed: () => timerBloc.add(Resume()),
        ),
        FloatingActionButton(
          child: Icon(Icons.replay),
          onPressed: () => timerBloc.add(Reset()),
        ),
      ];
    }

    //se il timer è scaduto visualizzerò solamente il pulsante di reset.
    if (currentState is Finished) {
      return [
        FloatingActionButton(
          child: Icon(Icons.replay),
          onPressed: () => timerBloc.add(Reset()),
        ),
      ];
    }

    return [];
  }
}
