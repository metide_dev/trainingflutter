//definiamo una classe che gestisca la durata dell'intervallo di tempo.
class Ticker{
  //il parametro tick mi rappresenta la durata dell'intervallo.
  Stream<int> tick({int ticks}){
    return Stream.periodic(Duration(seconds: 1), (x) => ticks - x - 1)
           .take(ticks);
  }
}