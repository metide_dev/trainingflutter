import 'package:equatable/equatable.dart';
import 'package:flutter_timer/timer.dart';

//definiamo una classe astratta per lo stato.
abstract class TimerState extends Equatable{
  
  final int duration;

  const TimerState(this.duration);

  @override
  List<Object> get props => [duration];

}

//definiamo lo stato Ready immutabile da notare l'uso di const davanti al costruttore.
class Ready extends TimerState{

  const Ready(int duration) : super(duration);

  @override
  String toString() => 'Paused { duration : $duration}';
  
}

//definiamo lo stato Running immutabile da notare l'uso di const davanti al costruttore.
class Running extends TimerState {

  const Running(int duration) : super(duration);

  @override
  String toString() => 'Paused { duration : $duration}';

}

//definiamo lo stato Running immutabile da notare l'uso di const davanti al costruttore.
class Paused extends TimerState {
  const Paused(int duration) : super(duration);

  @override
  String toString() => 'Paused { duration: $duration }';
}

class Reseted extends TimerState {

  const Reseted(int duration) : super(duration);
  
  @override
  String toString() => 'Paused { duration: $duration }';
}

//definiamo lo stato Finished immutabile da notare l'uso di const davanti al costruttore.
class Finished extends TimerState {

  const Finished() : super(0);

}