import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter_timer/ticker.dart';
import 'timer_event.dart';
import 'timer_state.dart';
import 'package:meta/meta.dart';


/*
 * 
 *  Rappresenta il blocco. Nel Flutter Bloc Pattern un blocco gestisce gli stati e gli eventi.
 *  In input avremmo sempre un evento e in uscita uno stato da mandare alla widget.
 */
class TimerBloc extends Bloc<TimerEvent, TimerState> {

  //rappresenta la durata del timer ovvero 60 secondi.
  final int _duration = 60;

  final Ticker _ticker;

  StreamSubscription<int> _tickerSubscription;

  TimerBloc({@required Ticker ticker})
      : assert(ticker != null),
        _ticker = ticker;

  @override
  TimerState get initialState => Ready(_duration);

  /*
   * Verifica gli eventi che gli vengono notificati.
   * Esegue una mappatura dell'evento in uno stato (quello che poi dovrà)
   * essere ritornato alla widget.
   * 
   * Gli stati sono inseriti in un flusso (Stream) e la funzione è generatrice.
   * Questo significa che ogni volta che viene notificato un evento viene sempre
   * restituito un valore all'interno dello stream.
   */
  @override
  Stream<TimerState> mapEventToState(
    TimerEvent event,
  ) async* {
    if (event is Start) {
      yield* _mapStartToState(event);
    } else if (event is Tick) {
      yield* _mapTickToState(event);
    } else if (event is Pause) {
      yield* _mapPauseToState(event);
    }else if (event is Resume) {
      yield* _mapResumeToState(event);
    }else if (event is Reset){
      yield* _mapResetToState(event);
    }
  }

  /*
   * Se il timer è in esecuzione allora lo porto allo stato di partenza.
   * ritornando reimpostandolo con il valore iniziale.
  */
  Stream<TimerState> _mapResetToState(Reset reset) async* {
    
    if (state is Running ) {
      _tickerSubscription?.cancel();
      yield Ready(_duration);
    }else if (state is Paused ) {
      _tickerSubscription?.cancel();
      yield Ready(_duration);
    }

  } 

  /*
   * Funzione Generatrice, la funzione prende in pasto l'evento di start.
   * ritorna nello stream un valore che è lo stato di running.
   */
  Stream<TimerState> _mapStartToState(Start start) async* {
    yield Running(start.duration);
    _tickerSubscription?.cancel();
    _tickerSubscription = _ticker
        .tick(ticks: start.duration)
        .listen((duration) => add(Tick(duration: duration)));
  }

  /*
   *  Sono nel ciclo di clock quindi se il contatore è meggiore di zero
   *  allora ritorno lo stato di running con la durata corrente altrimenti
   *  se il contatore è pari a zero il timer ha finito e quindi ritorno lo
   *  stato Finished. 
   */
  Stream<TimerState> _mapTickToState(Tick tick) async* {
    //mette nello stream lo stato running se il timer non è ancora scaduto altrimenti lo
    //stato finished (Finito)
    yield tick.duration > 0 ? Running(tick.duration) : Finished();
  }

  /*
   * Se sono in uno stato running allora posso mettere in pausa 
   * e bloccare momentaneamente la sottoscrizione degli eventi.
   */
  Stream<TimerState> _mapPauseToState(Pause pause) async* {
    if (state is Running) {
      //mette in pausa la sottoscrizione degli eventi
      //non possono più essere eseguite delle subscribe all'evento.
      _tickerSubscription?.pause(); 
      //inserisce nello stram lo stato pausa.
      yield Paused(state.duration);
    }
  }

  /*
   * Se lo stato corrente è in pausa allora 
   * posso riprendere ad eseguire il contatore riabilitando lo sottoscrizione 
   * degli eventi.
   */
  Stream<TimerState> _mapResumeToState(Resume resume) async* {
    if (state is Paused) {
      _tickerSubscription?.resume();
      yield Running(state.duration);
    }
  }

  /*
  * cancello la lista delle subscribe
  */
  @override
  Future<void> close() {
    _tickerSubscription?.cancel();
    return super.close();
  }
}
