import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

//definiamo la classe generica che definisce un evento.
abstract class TimerEvent extends Equatable {
  const TimerEvent();

  @override
  List<Object> get props => [];
}

//definaimo l'evento di start del timer.
class Start extends TimerEvent {
  final int duration;

  const Start({@required this.duration});

  @override
  String toString() => "Start { duration: $duration }";
}

//definiamo l'evento di pausa del timer
class Pause extends TimerEvent {}

//definiamo l'evento di ripresa del timer
class Resume extends TimerEvent {}

//deiniamo l'evento di Reset del timer
class Reset extends TimerEvent {}


class Tick extends TimerEvent {
  final int duration;

  const Tick({@required this.duration});

  @override
  List<Object> get props => [duration];

  @override
  String toString() => "Tick { duration: $duration }";
}