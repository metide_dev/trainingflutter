import 'package:flutter/material.dart';
import 'package:quiz_app/model/question.dart';

class QuizApp extends StatefulWidget {
  @override
  _QuizAppState createState() => _QuizAppState();
}

class _QuizAppState extends State<QuizApp> {

  int _currentQuestionIndex = 0;

  List questionBank = [
    Question.name("Question 1", true),
    Question.name("Question 2", false),
    Question.name("Question 3", true),
    Question.name("Question 4", false),
    Question.name("Question 5", true)
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('True Citizen'),
        centerTitle: true,
        backgroundColor: Colors.blueGrey,
      ),
      backgroundColor: Colors.blueGrey,
      body: Builder(
        builder: (BuildContext context) => Container(
          child: Column(
            children: <Widget>[
              Center(
                child: Image.asset(  "images/flag.png",
                              width: 250,
                              height: 180,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(14.4),
                    border: Border.all(
                      color: Colors.blueGrey.shade400,
                      style: BorderStyle.solid,
                    ),
                  ),
                  height: 120.0,
                  child: Center(child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(questionBank.elementAt(_currentQuestionIndex).questionText,style: TextStyle(
                      fontSize: 16.9,
                    ),),
                  )),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RaisedButton(
                    onPressed: () => _prevQuestion(),
                    color: Colors.blueGrey.shade900,
                    child: Icon(Icons.arrow_back,color : Colors.white),
                  ),
                  RaisedButton(
                    onPressed: () => _checkAnswer(true,context),
                    color: Colors.blueGrey.shade900,
                    child: Text("TRUE",style: TextStyle(color : Colors.white)),
                  ),
                  RaisedButton(
                    onPressed: () => _checkAnswer(false,context),
                    color: Colors.blueGrey.shade900,
                    child: Text("FALSE",style: TextStyle(color : Colors.white),),
                  ),
                  RaisedButton(
                    onPressed: () => _nextQuestion(),
                    color: Colors.blueGrey.shade900,
                    child: Icon(Icons.arrow_forward,color : Colors.white),
                  ),
                ],
              ),
              Spacer(
              ),
            ],
          ),
        ),
      ),
    );
  }

  _checkAnswer(bool userChoise,BuildContext context){
    if(userChoise == questionBank[_currentQuestionIndex].isCorrect){

      final snackBar = SnackBar(  content : Text(("Correct!")),
                                  duration: Duration(microseconds: 500),
                                  backgroundColor: Colors.green,);
      Scaffold.of(context).showSnackBar(snackBar);
      this._updateQuestion();
    }else{
      final snackBar = SnackBar(content : Text(("Incorrect!")),
                                backgroundColor: Colors.red,
                                duration: Duration(microseconds: 500),);
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }

  _updateQuestion(){
    setState(() {
      _currentQuestionIndex = (_currentQuestionIndex + 1) % questionBank.length;
    });
  }

  _nextQuestion(){
    _updateQuestion();
  }
  _prevQuestion(){
    setState(() {
      _currentQuestionIndex = (_currentQuestionIndex - 1) % questionBank.length;
    });
  }

}
