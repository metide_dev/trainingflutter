// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// You can read about packages here: https://flutter.io/using-packages/
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(new MaterialApp(
    home: new PokemonAPI(),
  ));
}

class PokemonAPI extends StatefulWidget {
  @override
  PokemonAPIState createState() => new PokemonAPIState();
}

class PokemonAPIState extends State<PokemonAPI> {
  List data;
  var namesList = ['Michele', 'Andrea', 'Debora', 'Nicla', 'Stefano', 'Anna', 'Christian', 'Francesco']; //possessori futuri di un pokemon
  var pokemonData = [];

  Future<String> getData() async {
    //chiamata asincrona per eliminare i problemi riguardo alla connessione lenta
    // print(data);

    for (var i = 0; i < namesList.length; i++) {
      var randomNumber = Random().nextInt(806); //creazione numbero generato casualmente
      http.Response response = await http.get(
          Uri.encodeFull("https://pokeapi.glitch.me/v1/pokemon/" + randomNumber.toString())); //encodeFull serve per eliminare i problemi con i caratteri speciali all'interno dell'url

      data = jsonDecode(response.body);
      this.setState(() {
        pokemonData.add(data); //agguinta pokemon generato all lista
      });
    }
    print(pokemonData[0][0]);
  }

  @override
  void initState() {
    this.getData();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Pokemon Generator'),
      ),
      body: new ListView.builder(
        itemCount: pokemonData == null ? 0 : pokemonData.length,
        //controllo nel caso la lunghezza di data sia null (lunghezza zero) o abbia contenuto
        itemBuilder: (BuildContext context, int index) {
          return new Card(
            // color: pokemonData[0][index]['legendary'] = true ? Colors.yellowAccent : Colors.white,
            child: Padding(
              padding: EdgeInsets.only(
                  top: 36.0, left: 6.0, right: 6.0, bottom: 6.0),
              child: ExpansionTile(
                  title: Text(namesList[index]),
                  backgroundColor: pokemonData[index][0]['legendary'] == true ? Colors.purple : Colors.white,
                  children: [
                    ListTile(title: Text('Pokemon Name: ${pokemonData[index][0]['name']}'),),
                    ListTile(title: Text('Height: ${pokemonData[index][0]['height']}'),),
                    ListTile(title: Text('Weight: ${pokemonData[index][0]['weight']}'),),
                    ListTile(title: Text('Generation: ${pokemonData[index][0]['gen']}'),),
                    ListTile(title: Text('Description: ${pokemonData[index][0]['description']}'),),
                    Image.network(pokemonData[index][0]['sprite'], height: 200,),
                  ]
              ),
            ),
          );
        },
      ),
    );
  }
}
